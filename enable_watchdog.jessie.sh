#!/bin/bash

# Based on:
# https://diode.io/raspberry%20pi/running-forever-with-the-raspberry-pi-hardware-watchdog-20202/

if [ 'root' != $( whoami ) ] ; then
  echo "Please run as root!"
  exit 1;
fi

sudo apt-get install watchdog
echo 'dtparam=watchdog=on' | sudo tee --append /boot/config.txt

echo '# enable_watchdog.sh:
watchdog-device = /dev/watchdog
watchdog-timeout = 15
max-load-1 = 24
# interface = wlan0
# interface = eth0
' | sudo tee --append /etc/watchdog.conf

sudo systemctl enable watchdog

echo "Reboot to enable watchdog!"
