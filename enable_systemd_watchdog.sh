#!/bin/bash

# Based on:
# https://bends.se/?page=notebook/sbc/raspberry-pi/hw-watchdog

# If you are using a Pi 2 or older, set dtparam=watchdog=on in /boot/config.txt
# Add to /boot/cmdline.txt bcm2835-wdt.nowayout=1

if [ 'root' != $( whoami ) ] ; then
  echo "Please run as root!"
  exit 1;
fi

if [ 0 -eq $( grep -c 'RuntimeWatchdogSec=14' /etc/systemd/system.conf ) ]; then

  echo '

# RPi doesnt support > 15
RuntimeWatchdogSec=14

# Reboot the system if nothing contacts the watchdog within this time. Default off.
RebootWatchdogSec=5min

# How long to wait for each program to exit when shutting down or rebooting. Defaults to 90s
# DefaultTimeoutStopSec=90s

' | sudo tee --append /etc/systemd/system.conf

  sudo systemctl daemon-reload
fi

echo "* Changing boot up parameters (from setup.sh)"
cp /boot/firmware/cmdline.txt /boot/firmware/cmdline.txt.backup2
echo "$(cat /boot/firmware/cmdline.txt) bcm2835-wdt.nowayout=1" > /boot/firmware/cmdline.txt

echo "Watchdog with autoreset enabled." > /home/pi/watchdog.txt
