#!/bin/bash

echo "Warning: this will not ask questions, just go for it. Backups are made where it makes sense, but please don't run this on anything but a fresh install of Raspbian (bookworm [lite]). Run as root ( sudo ${0} )."

if [ 'root' != $( whoami ) ] ; then
  echo "Please run as root!"
  exit 1;
fi

apt update || { echo "Update failed"; exit 1; }

echo "*Removing some unneeded software..."
apt remove -y --purge logrotate dphys-swapfile rsyslog

echo "* Changing boot parameters."
cmdline="/boot/firmware/cmdline.txt"
grep ' ro ' $cmdline || {
  cp $cmdline $cmdline.backup
  echo "$(cat $cmdline) ro noswap fastboot" > $cmdline
}


echo "* Move random-seed to tmpfs and change service that inits it"
test -L /var/lib/systemd/random-seed || {
  rm /var/lib/systemd/random-seed && \
    ln -s /tmp/random-seed /var/lib/systemd/random-seed
  cp /lib/systemd/system/systemd-random-seed.service /etc/systemd/system/systemd-random-seed.service
  cat > /etc/systemd/system/systemd-random-seed.service << EOF
[Unit]
Description=Load/Save Random Seed
Documentation=man:systemd-random-seed.service(8) man:random(4)
DefaultDependencies=no
RequiresMountsFor=/var/lib/systemd/random-seed
Conflicts=shutdown.target
After=systemd-remount-fs.service
Before=first-boot-complete.target shutdown.target
Wants=first-boot-complete.target
ConditionVirtualization=!container
ConditionPathExists=!/etc/initrd-release

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStartPre=/bin/echo '' >/tmp/random-seed
ExecStart=/lib/systemd/systemd-random-seed load
ExecStop=/lib/systemd/systemd-random-seed save

TimeoutSec=30s
EOF
}

# echo "* Adjust rpi fake-hwclock script to remount"
# test -f /etc/cron.hourly/fake-hwclock || {
#   cp /etc/cron.hourly/fake-hwclock /etc/cron.hourly/fake-hwclock.backup
#   cat > /etc/cron.hourly/fake-hwclock << EOF
# #!/bin/sh
# #
# # Simple cron script - save the current clock periodically in case of
# # a power failure or other crash
 
# if (command -v fake-hwclock >/dev/null 2>&1) ; then
#   ro=$(mount | sed -n -e "s/^\/dev\/.* on \/ .*(\(r[w|o]\).*/\1/p")
#   if [ "$ro" = "ro" ]; then
#     mount -o remount,rw /
#   fi
#   fake-hwclock save
#   if [ "$ro" = "ro" ]; then
#     mount -o remount,ro /
#   fi
# fi
# EOF
# }


echo "* Setting up tmpfs for lightdm, in case this isn't a headless system."
ln -fs /tmp/.Xauthority /home/pi/.Xauthority
ln -fs /tmp/.xsession-errors /home/pi/.xsession-errors


echo "* Setting fs as ro in fstab (unless something is set ro already)"
if [ 0 -eq $( grep -c ',ro' /etc/fstab ) ]; then
  sed -i.bak "/boot/ s/defaults/defaults,ro/g" /etc/fstab
  sed -i "/ext4/ s/defaults/defaults,ro/g" /etc/fstab

  echo "
  tmpfs           /tmp             tmpfs   nosuid,nodev         0       0
  tmpfs           /var/log         tmpfs   nosuid,nodev         0       0
  tmpfs           /var/tmp         tmpfs   nosuid,nodev         0       0
  tmpfs           /var/lib/dhcp    tmpfs   nosuid,nodev         0       0
  tmpfs           /var/lib/NetworkManager/ tmpfs   nosuid,nodev         0       0
" >> /etc/fstab
fi


echo "* Modifying bashrc"
if [ 0 -eq $( grep -c 'mount -o remount' /etc/bash.bashrc ) ]; then
  cat ./bash.bashrc.addon >> /etc/bash.bashrc
fi


echo "* Set a DNS server, and keep networkmanager from attempting to write to resolv.conf"
if [ 0 -eq $( grep -c 'rc-manager=unmanaged' /etc/NetworkManager/NetworkManager.conf ) ]; then
  echo "[main]
plugins=ifupdown,keyfile
dns=none
rc-manager=unmanaged

[ifupdown]
managed=false

[device]
wifi.scan-rand-mac-address=no
  " > /etc/NetworkManager/NetworkManager.conf
  echo "nameserver 9.9.9.9" > /etc/resolv.conf
fi

touch /etc/bash.bash_logout
if [ 0 -eq $( grep -c 'mount -o remount' /etc/bash.bash_logout ) ]; then
  cat ./bash.bash_logout.addon >> /etc/bash.bash_logout
fi

echo "* Configuring kernel to auto reboot on panic."
echo "kernel.panic = 10" > /etc/sysctl.d/01-panic.conf

echo "* Disabling services that need to write to disk"
systemctl disable apt-daily.timer
systemctl disable apt-daily.service
systemctl disable apt-daily-upgrade.timer
systemctl disable apt-daily-upgrade.service
systemctl disable dpkg-db-backup.timer
systemctl disable dpkg-db-backup.service

echo "* Done! Reboot and hope it will come back up."
echo "This system is running readonly. Run rw to remount read/write, and ro to return to read-only." > /home/pi/readonly.txt
