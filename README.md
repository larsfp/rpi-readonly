# rpi-readonly

Make the Raspberry Pi Raspbian file system read-only.

Based on https://hallard.me/raspberry-pi-read-only/

## Why

This will mostly avoid SD-card wearout.

## Compatibility

* Tested OK on 2023-11 Bookworm. Old version re-named setup.sh.bullseye.
* Tested OK on 2022-01-28-raspios-bullseye-armhf-lite.img
* Old version named setup.sh.stretch - Tested OK on 2017-11-29-raspbian-stretch*.img
* Old version named setup.sh.jessie - Tested OK on 2017-04-10-raspbian-jessie*.img

## Tricks for certain apps:

* Apache2, edit logpath in /etc/apache2/env
* Chromium-browser, use --user-data-dir /tmp/something
* Lightdm - fixed via setup.sh
* Rpi-Cam-Web-Interface: mount /var/www/html/media/ on tmpfs or external rw disk

## Usage after setup

* If you need to change anything, run rw to remount file system read/write. Then ro to make it read only. If you are unable to remount ro, a reboot is the only solution.
* Logs can be read normally with journalctl, but will not be saved to disk.

See also enable_watchdog.sh for remote systems.

## Differences between this and Raspberry Pis official "Overlay File System"

+ rpi-readonly shows which mode you are in, overlay is invisible.
+ rpi-readonly allows switching back and forth without a reboot, overlay does not.

## TODO

* Check if https://github.com/MarkDurbin104/rPi-ReadOnly has anything I'm missing.

## Enable_Watchdog

The RPi has a hardware watchdog that can reset the system if it becomes unresponsive. Very practical if the RPi is in a remote area, and totally safe when the file system is read-only.

To enable this run ```enable_watchdog.sh```. If you also want it to check the network (recommended), make sure to uncomment the interface line in /etc/watchdog.conf.
